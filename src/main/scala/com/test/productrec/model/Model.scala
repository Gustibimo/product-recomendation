package com.test.productrec.model


  /**
    * This class holds information about uid-pid relationship score weighted by date (number of days)
    *
    * @param weightedScore effective score weighted by time (formula = score * 0.95 pow(days) )
    * @param uid user ID (e.g 1234, 2329)
    * @param pid product ID
    * @param days created day (e.g 10 days ago, today, 3 days ago)
    */
case class PrecomputedCalculation(uid: Int, pid:Int, weightedScore: Long, days: Int)

/**
  * This class holds information about Top 5 list of recomendation result
  * @param pid product id
  */

case class TopFiveRecomendation(pid:Int)



