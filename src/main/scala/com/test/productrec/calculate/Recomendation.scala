package com.test.productrec.calculate


import com.test.productrec.model.{PrecomputedCalculation, TopFiveRecomendation}

object Recomendation {

  /**
    * This function will calculate weight of each user based on score created date.
    * steps to calculate weight of each article :
    * 1. calculate weighted score calculation with [[initialize()]]
    * 2. calculate Top 5 recomended products with [[recomendProducts()]]
    *
    * @param userPreferencePath user preference with uid,pid,score and timestamp
    * @param productScorePath pid and score of product
    * @return [[List]] of [[PrecomputedCalculation]] and [[List]] of [[]]
    */
  def initialize(userPreferencePath:String, productScorePath:String): List[PrecomputedCalculation] = ???


  /**
    *
    * @param uid user ID
    * @return [[List]] of [[TopFiveRecomendation]]
    */
  def recomendProducts(uid:Int): List[TopFiveRecomendation] = ???



}
